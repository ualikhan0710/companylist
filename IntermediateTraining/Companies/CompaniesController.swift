//
//  ViewController.swift
//  IntermediateTraining
//
//  Created by Уали on 07.09.2018.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import UIKit
import CoreData
class CompaniesController: UITableViewController {
    
    var companies = [Company]()//empty array
    
    @objc private func doWork(){
        print("trying to do work...")
        
        CoreDataManager.shared.persistentConteiner.performBackgroundTask({ (backgroundContext) in
            
            (0...5).forEach { (value) in
                print(value)
                let company = Company(context: backgroundContext)
                company.name = String(value)
            }
            
            do {
                try backgroundContext.save()
                
                DispatchQueue.main.async {
                    self.companies = CoreDataManager.shared.fetchCompanies()
                    self.tableView.reloadData()
                    
                    
                }
                
            }catch let err{
                print("Failed to save:", err)
            }
            
        })
        
        
        // GCD Grand Central  Dispatch
        DispatchQueue.global(qos: .background).async {
            
            
            
             // creating some Company objects on a background thread
//            let context = CoreDataManager.shared.persistentConteiner.viewContext
        }
    }
    
    //let's do some tricky updates woth core data
    @objc private func doUpdates(){
        print("trying to update companies on a backgound context")
        
        CoreDataManager.shared.persistentConteiner.performBackgroundTask { (backgoundContext) in
            let request: NSFetchRequest<Company> = Company.fetchRequest()
            
            do {
                 let companies = try backgoundContext.fetch(request)
                companies.forEach({ (company) in
                    print(company.name ?? "")
                    company.name = "B: \(company.name ?? "" )"
                    
                })
                
                
                do {
                    try  backgoundContext.save()
                    // let's try to update the UI after a save
                    
                    DispatchQueue.main.async {
                        
                        //reset will forget all of the objects you've fetch before
                        CoreDataManager.shared.persistentConteiner.viewContext.reset()
                        
                        //you don't want to refetch everything if you're just simply update one or two companies
                        self.companies = CoreDataManager.shared.fetchCompanies()
                        
                        //is there a way to just merge the changes that you made into the main view context?
                        self.tableView.reloadData()
                        
                        
                    }
                    
                    
                }catch let saveErr{
                    print("Failed to save on background:", saveErr)
                }
               
                
                
                
            } catch let err{
                print("Failed to fetch company background:", err)
            }
        }
    }
    
    @objc private func doNestedUpdates(){
        print("Trying to  perform nested updates now..")
        
        DispatchQueue.global(qos: .background).async {
            //we'll try to perform updates
            
            //we'llfirst construct a custom MOC
            let privateContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
            
            privateContext.parent = CoreDataManager.shared.persistentConteiner.viewContext
            
            //execute updates on privateContext now
            let request: NSFetchRequest<Company> = Company.fetchRequest()
            request.fetchLimit = 1
            do {
                 let companies  = try privateContext.fetch(request)
                
                companies.forEach({ (company) in
                    print(company.name ?? "")
                    company.name = "D: \(company.name ?? "" )"
                })
                
                do {
                    try privateContext.save()
                    //after save succeeds
                    
                    DispatchQueue.main.async {
                        do{
                            let context = CoreDataManager.shared.persistentConteiner.viewContext
                            
                            if context.hasChanges{
                              try  context.save()
                            }
                             
                            self.tableView.reloadData()
                            
                        } catch let finalSaveErr{
                            print("Fail to save main context:",finalSaveErr)
                        }
                    }
                    
                }catch let saveErr{
                 print("Failed to saveErr on privateContext:",saveErr)
                }
                
                
            }catch let fetchErr{
                print("Failed to fetch on privateContext:",fetchErr)
            }
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.companies = CoreDataManager.shared.fetchCompanies()
        
        navigationItem.leftBarButtonItems = [
            UIBarButtonItem(title: "Reset", style: .plain, target: self, action: #selector (handleReset)),
            UIBarButtonItem(title: "Nested updates", style: .plain, target: self, action: #selector (doNestedUpdates))
        ]
        
        view.backgroundColor = .white
        
        navigationItem.title = "Companies"
        
        tableView.backgroundColor = UIColor.darkBlue
//        tableView.separatorStyle = .none
        tableView.separatorColor = .white
        tableView.tableFooterView = UIView() //blank UIView
        
        tableView.register(CompanyCell.self, forCellReuseIdentifier: "cellId")
        
        setupAddButtonInNavBar(selector: #selector(handleAddCompany))
        
//        navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add").withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(handleAddCompany))
        
    }
    
    @objc private func handleReset(){
        print("Attempting to delete all core data objects")
        
        let context = CoreDataManager.shared.persistentConteiner.viewContext
        
        
        let batchDeleteRequest =  NSBatchDeleteRequest(fetchRequest: Company.fetchRequest() )
        
        do {
            try context.execute(batchDeleteRequest)
            
            //upon deletion from core data succeeded
            var indexPathsToRemove = [IndexPath]()
         
            for (index, _) in companies.enumerated(){
                let indexPath = IndexPath(row: index, section: 0)
                indexPathsToRemove.append(indexPath)
             }
            companies.removeAll()
            tableView.deleteRows(at: indexPathsToRemove, with: .left)
            }
        catch let delErr{
            print("Failed to delete objects from Core Data:", delErr)
        }
    }
    
    @objc func handleAddCompany(){
        print("Adding company")
        
        let createCompanyController = CreateCompanyController()
        
        let navController = UINavigationController(rootViewController: createCompanyController)
        
        createCompanyController.delegate = self
        
        present(navController, animated: true, completion: nil)
    
    }
}

