//
//  CompaniesController+CreateCompany.swift
//  IntermediateTraining
//
//  Created by Уали on 19.09.2018.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import UIKit


extension CompaniesController: CreateCompanyColtrollerDelegate{
  
    
    func didEditCompany(company: Company) {
        //Update my tableView somehow..
        let row = companies.index(of: company)
        
        let reloadIndexPath = IndexPath(row: row!, section: 0)
        tableView.reloadRows(at: [reloadIndexPath], with: .middle)
    }
    
    func didAddCompany(company: Company) {
        //1 Modiffy your array
        companies.append(company)
        
        
        //2 insert new index path into tableVIew
        let newIndexPath = IndexPath(row: companies.count - 1, section: 0)
        tableView.insertRows(at: [newIndexPath], with: .automatic)
    }
    
    //specify your extencion methods here...
    
}
 
