//
//  EmployeeType.swift
//  IntermediateTraining
//
//  Created by Уали on 9/27/18.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import Foundation
enum EmployeeType:String {
    case Executive
    case SeniorManagement = "Senior Management"
    case Staff
    case Intern
}
