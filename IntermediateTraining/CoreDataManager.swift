//
//  CoreDataManager.swift
//  IntermediateTraining
//
//  Created by Уали on 11.09.2018.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import CoreData

struct CoreDataManager {
    static let shared = CoreDataManager()
    //will live forever as long as your application is still alive,it's properties will too
    
    let persistentConteiner : NSPersistentContainer = {
        //initialization of out Core Data
        let container  = NSPersistentContainer(name: "IntermediateTrainingModels")
        container.loadPersistentStores { (storeDescription, err) in
            if let err = err{
                fatalError("Loading of store failed : \(err)")
            }
        }
        return container
    }()
    
    func fetchCompanies() -> [Company]{
        let context = CoreDataManager.shared.persistentConteiner.viewContext
        
        let fetchRequest = NSFetchRequest<Company>(entityName: "Company")
        do {
            let companies = try context.fetch(fetchRequest)
            return companies
        } catch let fetchErr{
            print("Failed to fetch companies:",fetchErr)
            return []
        }
        
    }
    
    func createEmployee(employeeName: String, employeeType : String, birthday : Date ,company: Company)-> (Employee?,Error?){
        let context = persistentConteiner.viewContext
        
        //create employee
        let employee = NSEntityDescription.insertNewObject(forEntityName: "Employee", into: context) as! Employee
        
        employee.company = company
        employee.type = employeeType
        
        //lets check company is setup correctly
//        let company = Company(context: context)
//        company.employees
//
//        employee.company
        
        employee.setValue(employeeName, forKey: "name")
        
        let employeeInformation = NSEntityDescription.insertNewObject(forEntityName: "EmployeeInformation", into: context) as! EmployeeInformation
        
        employeeInformation.taxId = "456"
        employeeInformation.birthday = birthday
//        employeeInformation.setValue("456", forKey: "taxId")
        employee.employeeInformation = employeeInformation
        
        do{
            try context.save()
            //save succeeds
            return (employee as? Employee,nil)
        }catch let saveErr{
            print("Failed to create employee",saveErr )
            return saveErr as! (Employee?, Error?)
        }
        
    }
}












