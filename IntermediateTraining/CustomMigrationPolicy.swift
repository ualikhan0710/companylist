//
//  CustomMigrationPolicy.swift
//  IntermediateTraining
//
//  Created by Уали on 10/3/18.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import CoreData

class CustomMigrationPolicy: NSEntityMigrationPolicy {
    //type our transformation function here in just a bit
    
    @objc func transformNumEmployees(forNum: NSNumber) ->String {
        if forNum.intValue < 150 {
            return "small"
        }else{
            return "very large"
        }
    }
}
