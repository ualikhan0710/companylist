//
//  UIViewController+Helpers.swift
//  IntermediateTraining
//
//  Created by Уали on 20.09.2018.
//  Copyright © 2018 Уалихан. All rights reserved.
//

import UIKit

extension UIViewController {
    
    //my extension/helper methods
    
    func setupAddButtonInNavBar(selector: Selector){
            navigationItem.rightBarButtonItem = UIBarButtonItem(image: #imageLiteral(resourceName: "add") , style: .plain, target: self, action: selector)
    }
    
    func setupCancelButton(){
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(handleCancelModal))
    }
    
    
    @objc func handleCancelModal(){ 
            dismiss(animated: true, completion: nil)
    }
    
    
    func setupLightBlueBackgroundView(height: CGFloat) ->UIView{
        let lightBlueBackgroudView = UIView()
        lightBlueBackgroudView.backgroundColor = UIColor.lightBlue
        lightBlueBackgroudView.translatesAutoresizingMaskIntoConstraints = false
        
        view.addSubview(lightBlueBackgroudView)
        
        lightBlueBackgroudView.topAnchor.constraint(equalTo: view.topAnchor).isActive = true
        lightBlueBackgroudView.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        lightBlueBackgroudView.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        lightBlueBackgroudView.heightAnchor.constraint(equalToConstant: height).isActive = true
        
        return lightBlueBackgroudView
    
    }
}
